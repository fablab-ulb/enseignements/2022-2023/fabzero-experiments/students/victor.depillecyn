//BYU Cross-Axis Flexural pivot Flexlink reverse engineering script (https://www.compliantmechanisms.byu.edu/flexlinks)

//Author: Victor De Pillecyn

//Last modified: 08/03/2023

//License: Creative Commons (CC) BY
/////////////////////////////////////////////////

//ANCHOR parameters
UNITS = 4;
UNIT_WIDTH = 7.8;
UNIT_HEIGHT = 9.6/2;
HOLE_RADIUS = 2.55;

//FLEXLINK_PIVOT parameters
UNIT_NUMBER = 2;
UNIT_DISTANCE = 1.5*UNIT_WIDTH*UNITS;

//BEAM parameters
BEAM_WIDTH = UNIT_WIDTH/5;
BEAM_HEIGHT = UNIT_HEIGHT/4;
//Beam length calculated using Pythagorean theorem
BEAM_LENGTH = sqrt(UNIT_DISTANCE^2 + (UNIT_WIDTH*UNITS)^2);
//Beam angle calculated using the right-angled triangle tangens property 
BEAM_ANGLE = atan(UNIT_DISTANCE/(UNIT_WIDTH*UNITS-UNIT_WIDTH/UNITS));

// Other parameters
error=0.01;
$fn = 100;


// Code to generate two anchorparts
//anchorpart(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS);
//translate([0, UNIT_DISTANCE, 0])anchorpart(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS);

module anchorpart(units, unit_width, unit_height, hole_radius) { 
difference(){
    hull(){
        for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height, r=unit_width/2, center=true);
        }
    }
    for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height+error, r=hole_radius, center=true);
    }
}};


//Code to generate the crosssectional beams
//beampart(UNIT_WIDTH, UNIT_HEIGHT, UNIT_DISTANCE, BEAM_WIDTH, BEAM_HEIGHT, BEAM_LENGTH, BEAM_ANGLE);

module beampart(unit_width, unit_height, unit_distance, beam_width, beam_height, beam_length, beam_angle){
    color([1,0,0])union(){
        translate([-unit_width/2,0, unit_height/2])rotate([180, 0, beam_angle])cube([beam_length-1, beam_width, beam_height]);
        
    mirror([0,0,1])translate([-unit_width/4, unit_distance, unit_height/2])rotate([180, 0, -beam_angle])cube([beam_length-2, beam_width, beam_height]);
    }};

// If I generate just the beams and anchors separatily, the beams stick out in the holes of the anchors, I thus needed to generate the beams, union them with the anchors, and then perforate the anchors

//Code to generate a complete Flexlink pivot
flexlink_pivot(UNITS, UNIT_WIDTH, UNIT_HEIGHT, UNIT_DISTANCE, HOLE_RADIUS, BEAM_WIDTH, BEAM_HEIGHT, BEAM_LENGTH, BEAM_ANGLE);

module flexlink_pivot(units, unit_width, unit_height, unit_distance, hole_radius, beam_width, beam_height, beam_length, beam_angle){
    difference(){
            union(){
                beampart(UNIT_WIDTH, UNIT_HEIGHT, UNIT_DISTANCE, BEAM_WIDTH, BEAM_HEIGHT, BEAM_LENGTH, BEAM_ANGLE);
                hull(){
                    for (i = [0:units-1]){
                        translate([unit_width*i, 0, 0])cylinder(h=unit_height, r=unit_width/2, center=true);
                    }
                }
                hull(){
                    for (i = [0:units-1]){
                        translate([unit_width*i, UNIT_DISTANCE, 0])cylinder(h=unit_height, r=unit_width/2, center=true);
                    }
                }
            }
        for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height+error, r=hole_radius, center=true);
        for (i = [0:units-1]){
            translate([unit_width*i, UNIT_DISTANCE, 0])cylinder(h=unit_height+error, r=hole_radius, center=true);
      }}}}