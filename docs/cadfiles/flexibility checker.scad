//Beam flexibility checker

//Author: Victor De Pillecyn

//Last modified: 08/03/2023

//License: Creative Commons (CC) BY
/////////////////////////////////////////////////

//BEAM parameters
BEAM_WIDTH = 1;
BEAM_LENGTH = 50;

// Other parameters
$fn = 100;

for (i = [0:5]){
            translate([0,i*5,0])cube([BEAM_LENGTH, BEAM_WIDTH+i*0.5, BEAM_WIDTH+i*0.5]);
};