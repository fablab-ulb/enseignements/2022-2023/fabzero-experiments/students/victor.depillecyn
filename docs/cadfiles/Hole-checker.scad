//Hole radius checker

//Author: Victor De Pillecyn

//Last modified: 08/03/2023

//License: Creative Commons (CC) BY
/////////////////////////////////////////////////


//ANCHOR parameters
UNITS = 7;
UNIT_WIDTH = 7.8;
UNIT_HEIGHT = 9.6/2;
HOLE_RADIUS = 2.2;
// Other parameters
error=0.01;
$fn = 100;

// Code to generate a part with increasing hole radius (0.1)
anchorpart(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS);

module anchorpart(units, unit_width, unit_height, hole_radius) { 
difference(){
    hull(){
        for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height, r=unit_width/2, center=true);
        }
    }
    for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height+error, r=hole_radius+(i*0.1), center=true);
    }
}};