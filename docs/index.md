# What is this blog for?

Hello everyone!

In this blog, I will document my first steps into the world of digital fabrication in a concise way. The structure will be that of the ULB university course I am following, called [How To Make (almost) Any Experiments](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/). Every week another topic will be investigated:

- [Week 1 -- Documenting and project managing](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn/fabzero-modules/module01/)

- [Week 2 -- Computer-Aided Design](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn/fabzero-modules/module02/)

- [Week 3 -- 3D Printing](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn/fabzero-modules/module03/)

- [Week 4 -- Selected Fablab tools: Microcontrollers and laser cutting](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn/fabzero-modules/module04/)

- [Week 5 -- Group dynamics and final project](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn/fabzero-modules/module05/)


## My personal information fact sheet

<figure markdown>
  !['An image of my face not smiling'](images/personal_image.JPG "An image of my neutral face"){width="200px" align="right"}
</figure>
<figcaption align = "right"> An image of my neutral face</figcaption>

Full name: **Victor De Pillecyn**

Date of birth: 9<sup>th</sup> of March 1998

City of residence: Schaarbeek, Brussels

Email: victor.de.pillecyn@ulb.be

Languages: English, French, Dutch

### Academic career  

- Sept 2021 - present: **Master in Bio-informatics and Modelling** @ULB University, Brussels  

- 2019 - 2021: **Master of Science in Bio-Engineering** with an Agrobiotechnology specialization @VUB University, Brussels  

- 2016 - 2019: Bachelor of Science in Bio-Engineering @VUB University, Brussels

### What keeps me positive in this cruel world?  
- Acquire new skills and gain knowledge about new domains from passionate people, as well as transmitting my own expertise to others.
- Playing basketball, enjoying a coffee or a beer with anyone wanting to join, going sailing, playing board games, ...
- Taking small individual steps to reduce my carbon footprint (going vegetarian, adopting zero-waste and minimalism reflexes, ...).

## My starting skills, traits and knowledge
- Good programming skills in Python & R, as well as notions of basic CLI commands.
- Experience with using Markdown language in an IDE (Virtual studio code, Jupyter notebook, Atom, ...).
- Basic knowledge of electromagnetism and had some basic practical courses about electronics and the use of a breadboard. An insatiable curiosity, a longer-than-average patience, a strong determination but a clumsy way of doing things.

## My learning goals
- Being able to design a part with computer-aided design software.
- Learn how to work with a 3D printer, a plasma cutter and a CNC machine.
- Work with 3 different types of materials: wood, plastic and metal.
- Finish a project that uses an electronic component.
- Have lunch with at least 3 people in the class.