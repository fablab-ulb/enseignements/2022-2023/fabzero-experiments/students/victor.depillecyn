# Week 3 -- 3D printing

This week, I 3D printed the Flexlink part I designed last week.

## 1. Parameter optimization

As you may have seen, my OpenSCAD file has a lot of parameters, for which the values were either arbitrarily chosen or based on [the dimensions of LEGO blocks found online](../fabzero-modules/images/Lego_dimensions.svg.png).

<figure markdown>
  !['Designed Cross-Axis Flexural Pivot with used dimensions'](../fabzero-modules/images/Dimensions.png "Designed Cross-Axis Flexural Pivot with used dimensions"){width="1000px"}
</figure>

To more precisely determine the value I should use for the three most important parameters (hole_radius, beam_height and beam_width), I first designed and printed the following two measuring tools.

### 1.1 Variable hole radius tool

To determine the accurate hole radius to use so the part is compatible with LEGO sticks, I quickly reused and adapted my previously mentioned anchor part module to generate an anchor part with variable hole radii. As I knew from the LEGO dimensions that the hole should have a radius of around 2.5 mm, I chose the smallest hole to have a radius of 2.2 mm and increased the radius by 0.1 mm for the following 6 holes. My code and an image of the resulting piece can be found below.

```
//Hole radius checker

//Author: Victor De Pillecyn

//Last modified: 08/03/2023

//License: Creative Commons (CC) BY
/////////////////////////////////////////////////


//ANCHOR parameters
UNITS = 7;
UNIT_WIDTH = 7.8;
UNIT_HEIGHT = 9.6/2;
HOLE_RADIUS = 2.2;
// Other parameters
error=0.01;
$fn = 100;

// Code to generate a part with increasing hole radius (0.1)
anchorpart(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS);

module anchorpart(units, unit_width, unit_height, hole_radius) { 
difference(){
    hull(){
        for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height, r=unit_width/2, center=true);
        }
    }
    for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height+error, r=hole_radius+(i*0.1), center=true);
    }
}};
```
<figure markdown>
  !['Image of the resulting hole checking tool'](../fabzero-modules/images/Hole%20checker.jpg "Variable hole tool")
</figure>

This tool revealed that both the holes with a radius of 2.5 mm and 2.6 mm are workable, with the 2.5 mm radius providing a tighter fit with LEGO bars. A weird artifact I obtained with the 2.5 mm radius hole, was that the LEGO bar didn't enter the hole in both directions, meaning it was not the same size on both ends. To conclude, **I decided to use a 2.55 mm hole radius for my Flexlink part**.

### 1.2 Beams of variable width

As mentioned in my previous chapter, the cross-sectional beams connecting the two anchor parts are crucial for two reasons:

1. They will be experiencing the most stress, and in repeating ways. They thus have to be sturdy enough.

2. Their flexibility dictates how well the part can be used:  How far you can displace the two anchor parts from each other?

I decided to test different thicknesses of cross-sectional beams to optimize this trade-off between flexibility and sturdiness. I printed multiple bars of length 50 mm, with a height and width of 1 to 3.5 mm with 0.5 mm increments. The following OpenSCAD code generated the beams you can see in the image below.

```
//Beam flexibility checker

//Author: Victor De Pillecyn

//Last modified: 08/03/2023

//License: Creative Commons (CC) BY
/////////////////////////////////////////////////

//BEAM parameters
BEAM_WIDTH = 1;
BEAM_LENGTH = 50;

// Other parameters
$fn = 100;

for (i = [0:5]){
            translate([0,i*5,0])cube([BEAM_LENGTH, BEAM_WIDTH+i*0.5, BEAM_WIDTH+i*0.5]);
};
```

<figure markdown>
  !['Image of the beams with variable widths'](../fabzero-modules/images/variable%20beams.jpg "Image of the beams with variable widths")
</figure>

The 1 mm bar is not only too fragile, but it also doesn't return to its original straight conformation after bending. The 2 mm and more bars had very low flexibility. **I thus decided to pick 1.5 mm as a good beam height and beam width**.


### 1.3 Supports

As one of the beams acts as an unsupported bridge, I decided to let the PrusaSlicer v2.5 generate supports which had to be removed.

## 2. The final Flexlink part

The part was printed with an infill of 15% with standard PLA settings. The following images illustrate the final result, with and without supports. The video shows the flexibility of the middle beams.

['Video of the first attempt of making a Flexlink'](https://youtube.com/shorts/H6awgjRV6d4?feature=share)

The result turned out pretty good, the beams are a bit less flexible than I hoped because I didn't account for the fact that two beams would increase the stiffness.

I searched online if any other people experimented with settings to make PLA more flexible, but all my search results turned out to be about flexible filaments, another type of material which unlike PLA does not shatter under a sudden force, but bends and returns to its original shape. 

To try and improve this without changing the printing material, I tried a second print where I increase the infill but make the beams a bit longer. The beams are more flexible, but feel more vulnerable to breaking, especially when bending the anchor parts toward each other.

!['Image of the second attempt of making a Flexlink'](../fabzero-modules/images/second%20attempt%20flexlink.jpg)

## 3. Group compliant mechanism

Together with [Louis Devroye](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/louis.devroye/) and [Thibault Leonard](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/leonard.thibault/), we joined our three compliant parts to create a **Don't-forget-your-keys-ringer**.

The idea of this tool is that you fix your keyring to Louis' [flexible hook, which can be easily mounted](https://youtube.com/shorts/l14TdNtt3Tk?feature=share) on [Thibault's flexible rod mounted on my Flexible rocker](https://youtu.be/bw_UXFCMQMc). If you fix the complete part to the wall next to your main door, such that your door hits the flexible rod when you open it, your keys wrinkle, reminding you not to forget them!

![Image of group compliant mechanism: Don't-forget-your-keys-ringer](../fabzero-modules/images/group%20compliant%20mechanism.jpg)

## My starting skills and knowledge on the topic

- Nothing! The closest I have been to 3D printing is regular paper printing. 

## What I learned on the topic
- The actual 3D printing takes way longer than I expected, so it pays off to optimize parameters with smaller prints before committing to a big print.
- 3D printers have a printing error
- Consider different 3D printing filaments depending on the material properties you want (f.e. PLA is not super flexible)
