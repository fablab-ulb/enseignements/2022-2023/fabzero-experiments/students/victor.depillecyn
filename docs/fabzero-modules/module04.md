# Week 4 -- Microcontrollers and laser cutting

This week, I learned two new Fablab tools: **Using microcontrollers** and **laser cutting**

## 1. Microcontrollers

**Microcontrollers are small and low-cost microcomputers**: They get inputs, process them and convert them to outputs. As we are working with electronic devices, the inputs and outputs are generally currents, exchanged between the microcontroller and its peripherals (f.e. sensors, LEDs, buttons, ...). **The hardware (= the physical components) and the software (= the programs) of microcontrollers can easily be adapted** to solve a specific problem.

### 1.1 Hardware

The microcontroller module I will be using is the **YD-RP2040**, which strongly resembles the Rasberry Pi pico. It contains headers (pins on the side) that allow us to connect peripherals via cables, instead of having to solder them. The YD-RP2040 also **contains an integrated and programmable blue LED, as well as an addressable RGB LED**. The former can only be turned on and off, while the latter can be set to any color. The USB-C port on the microcontroller can be linked to your computer to change the software.

### 1.2 Software

To change what the microcontroller will do once it is powered, it can be given instructions in different programming languages (Arduino, C+, Python, ...). An adequate interpreter then has to convert the written human-readable code into machine-readable code, to be sent to the chip.

#### 1.2.1 MicroPython in Thonny IDE

I will be **writing instructions in Python** in the Thonny IDE, which includes a microcontroller-specific Python interpreter called MicroPython. The steps to properly **connect the microcontroller to the Thonny IDE are**:

1. Install and launch Thonny (https://thonny.org/)
2. Go to *Tools* > *Options...* and select the *Interpreter* tab. Select the MicroPython (Rasberry Pi pico) interpreter and click *OK*.
3. Connect the microcontroller to your computer while holding the *boot* button.
4. Install the necessary drivers
5. OPTIONAL: If you get an error saying the microcontroller is busy, press CTRL+C to interrupt it
6. You can now write code and send it to the microcontroller to run.

### 1.3 Turning the YD-RP2040 into a stoplight

As a first project, I used the built-in addressable RGB LED to make a miniature stoplight. To do this, I first understood how the RGB LED can be operated by turning it into what is called a neopixel, using [the micropython neopixel library](https://docs.micropython.org/en/latest/esp8266/tutorial/neopixel.html). The port of the RGB LED (port 23) has to be specified and can be found on [the data sheet](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/). The color can then be adapted by specifying the amount of red, green and blue light the LED should emit, each by a value between 0 and 255. The code for the stoplight can be found here:

```
'''
Miniature W2812 adressable RGB LED stoplight on YD-RP2040 microprocessor

Author: Victor De Pillecyn

Date of last change: 22/03/2023

License: Creative Commons BY (CC-BY)
//////////////////////////////////////////////////////////////////////////////////
'''

# Import necessary packages
from machine import Pin
import neopixel
import time

# Initialise 1 neopixel on pin 23
np = neopixel.NeoPixel(machine.Pin(23), 1)

while True: # Repeat indefinitely
    np[0] = (255, 0, 0) # Indicate that first neopixel (=0 in python) should be fully red
    np.write() # Don't forget this line to make the LED change color
    time.sleep(1) # Microprocessors waits 1 second
    np[0] = (255, 30, 0) # Color orange
    np.write()
    time.sleep(1)
    np[0] = (0, 255, 0) # Color green
    np.write()
    time.sleep(1)
```

### 1.4 Turning the YD-RP2040 into a relative humidity change detector

As a second project, I used a sensor that can measure temperature and relative humidity to turn the microcontroller into a relative humidity change detector:

- If the **relative humidity around the sensor goes up** (when you breathe against it for instance), the addressable RGB **LED light turns red**.

- If the **relative humidity around the sensor goes down** (when you breathe against it for instance), the addressable RGB **LED light turns blue**.

- If the **relative humidity around the sensor doesn't change** (when you breathe against it for instance), the addressable RGB **LED light turns blue**.

#### 1.4.1 Hardware

Requirements:
- YD-RP2040 microprocessor
- DHT20 humidity and temperature sensor
- Cables
- Breadboard

Steps (**Do without the microcontroller powered!**):

1. Plug the DHT20 humidity and temperature sensor into the breadboard.
2. [**Read the datasheet**](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf) of the DHT20 humidity and temperature sensor to understand the different pins:

<figure markdown>
  !['Image from the DHT20 datasheet displaying the function of the different pins'](../fabzero-modules/images/DHT20%20sensor%20datasheet.png "DHT20 datasheet displaying the function of the different pins"){width="500px"}
</figure>

3. Use a cable (preferably not red/black), to **connect the SDA pin (number 2)** to an SDA port on the microcontroller (for instance number 0). **Connect the SCL pin (number 4)** to an SCL port on the microcontroller (for instance number 1). These two channels serve as the communication channel of the microcontroller and the sensor, with the language being the I2C communication protocol.

4. Use a red cable to **connect the power supply pin (number 1)** of the DHT20 sensor to the power supply port on the microcontroller (denoted with 3V3). **Connect the ground pin (number 3)** to a ground port on the microcontroller (denoted with GND).

5. When you have checked everything, **connect the microcontroller to your computer** in BOOT mode.

#### 1.4.2 Software 

Requirements:

- The dht20.py script from https://github.com/flrrth/pico-dht20
- The operating script (see mine further)

Now you have to upload a script to the microcontroller, with the instruction of what it should do. I wrote the following script, that uses a script to connect to a DHT20 sensor from Github [user flrrth](https://github.com/flrrth/pico-dht20).

```
'''
Relative humidity change indicator using DHT20 humidity and temperature sensor
on YD-RP2040 microprocessor

Author: Victor De Pillecyn

Date of last change: 22/03/2023

License: Creative Commons BY (CC-BY)
///////////////////////////////////////////////////////////////////:
'''
# Import necessary packages and modules
import neopixel
from time import sleep
from machine import Pin, I2C

# You will need to add the dht20.py file to the Rasberry pico from
# https://github.com/flrrth/pico-dht20
from dht20 import DHT20

# Set up The Serial Data (SDA) and Serial Clock (SCL) pins for I2C0 communication protocol
i2c0_sda = Pin(0)
i2c0_scl = Pin(1)
i2c0 = I2C(0, sda=i2c0_sda, scl=i2c0_scl)

# Initiate sensor
dht20 = DHT20(0x38, i2c0)

# Denote adressable RGB LED
np = neopixel.NeoPixel(machine.Pin(23), 1)

# Initialise parameters
measurement_number = 5 # Number of measurements/seconds to calculate the mean
measurement_array = [] # Array to keep the measurements stored
precision = 0 # Number of decimals wanted (careful, DHT20 only precise by 0.1)
change_needed = 1 # Change in relative humidity (%) needed to initialise light change

# Initial measurements
for i in range(measurement_number):
    measurement_array.append(int(dht20.measurements['rh'])) # Command to make a humidity measurement
    np[0] = ((i%2)*255, 0, (i%2)*255) # Blink between off and purple while setting up
    np.write() # Don't forget this line to make the LED change color
    sleep(1) # Sleep one second

# Check initial measurements by printing them
print(f'Initial measurements {measurement_array}')

while True:
    sleep(1)
    # Calculate mean of measurements and round off
    mean_hum = round(sum(measurement_array)/len(measurement_array), precision)
    print(f'mean humidity: {mean_hum}')
    current_hum = int(dht20.measurements['rh']) # Measure current relative humidity
    print(f'current humidity: {current_hum}')
    if current_hum > mean_hum + change_needed: # If current bigger than mean + change_needed
        np[0] = (255, 0, 0) # LED turns red
        np.write()
    elif current_hum < mean_hum - change_needed: # If current smaller than mean - change_needed
        np[0] = (0, 0, 255) # LED turns blue
        np.write()
    else:
        np[0] = (0, 255, 0) # LED turns green
        np.write()
    measurement_array.pop(0) # Remove first measurement from array
    measurement_array.append(current_hum) # Add current measurement to array

```


#### 1.4.3 The final result

It works! I filmed myself blowing on the sensor and the RGB LED changing color in [this video](https://youtu.be/ts8Tt1hlt5A).


### 1.5. Measuring eCO2 and tVOC concentrations

As a third project, I tried to use the SGP30 TVOC and eCO2 Sensor Module to measure the accumulation of carbon dioxide (CO2) and total concentration of volatile organic compounds (TVOC) in my bedroom while I sleep.

The first problem I encountered was that I was not able to find a proper datasheet for this exact sensor. Luckily, the ports are indicated on the sensor itself, but I would have liked to read what exactly it is measuring and how.

After setting up the sensor similarly to the temperature sensor, the second problem arose. I found [a script from safuya on github](https://github.com/safuya/micropython-sgp30) which is supposed to set up the connection between microcontroller and sensor and provide some easy functions to order a measurement. Unfortunately, I wasn't able to set up a connection, even when triple checking the ports and trying to use other ones. As I had a second Fablab tool workshop with new assignments, I decided not to spend more time trying to figure it out.

## 2. Laser cutter

The second machine I learned to work with is the laser cutter. **The laser cutter can cut multiple, but not all, materials!** This is because some materials can catch fire too easily, while others entail the formation of toxic fumes. The following table summarizes some materials that can('t) be used on the laser cutters of the ULB Fablab.

|Authorised|Discouraged|Forbidden
|----------|--------------|---------
|Acrylic plastics|Medium-density fibreboards|Polyvinyl chloride (PVC)
|Wood & Plywood|Polystyrene|Leather
|Textile|Polyethylene terephthalate (PET)|Vinyl
|Cardboard & paper|Fibrous composites|Phenol formaldehyde resins

The machine works by focusing the focal point of a laser on the material and moving it around to cut or engrave by burning the material. **Three important parameters can be adapted depending on the material used and the degree of burn one wants to cause (to cut or only to engrave)**:

- Laser intensity or power

- Speed at which the laser is moved

- Number of passes of the laser

Before cutting with a new machine or on a new type of material, you should reserve some time to **test different combinations of these 3 parameters** to see the results. A 4th parameter that is often important when you want to click different parts into each other (like a puzzle), is the kerf.

As a template, **all machines can take a *.svg* file as input** and follow the drawn lines and engrave filled-in surfaces. **Different colors can be used in the file to use different sets of the three parameters** on different regions. This can be used to make a test grid as follows:

!['Image of laser engraving/cutting test'](../fabzero-modules/images/laserengravingtest.png "Image of laser engraving/cutting test")

Laser cutting devices often also have a **fume extractor, a chiller and an air compressor** (to extinguish any potential flames around the laser cut by blowing air on the part) that you should **turn on before starting a cut**.

## 2.1 Laser cutting and engraving

To put the theory into practice, together with [Ibrahima Bah](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/ibrahima.bah) decided to start small and cut a small rectangle out of some plywood we found next to the used MUSE laser cutter and engrave text on it. We used part of an svg template found on [the boxes.py website](https://www.festi.info/boxes.py/index.html):

!['Image of the used template to test different parameters for laser cutting and engraving'](../fabzero-modules/images/laserprintingsvg.png "template to test different parameters for laser cutting and engraving")

## 2.2 Parameter optimization: Engraving with the MUSE

We started with a single pass at a speed of 100% and power of 80% for engraving, but this resulted in very black and burned letters (see first image further). We also learned that if you want your letters completely engraved instead of just the contours cut out, you have to color them in the svg file.

By reducing the power to 50% for engraving, the letters were much nicer (see second image further), although we think the laser was not optimally focused, leading to the burned contours.

## 2.3 Parameter optimization: Laser cutting with the MUSE

We started with a single pass at a speed of 50% and a power of 100% for cutting, but this didn't cut through the plywood and gave a burned appeal again (see first image).

Reducing the power to 70% and increasing the number of passes to 4 reduced the burning effect, but did not cut through the material (see second image). 

A third attempt with 8 passes at speed 50% and power 70% resulted in the rectangle being cut out (see third image).


!['Image of first attempt laser cutting and engraving'](../fabzero-modules/images/first%20attempt%20laser%20cutting.jpg "First attempt laser cutting")

!['Image of second attempt laser cutting and engraving'](../fabzero-modules/images/2nd%20attempt%20laser%20cutting.jpg "Second attempt laser cutting")

!['Image of third attempt laser cutting and engraving'](../fabzero-modules/images/3rd%20attempt%20laser%20cutting.jpg "Third attempt laser cutting")


## 2.4 Laser cutting project: A wooden Möbius strip

!['Möbius strip'](../fabzero-modules/images/mobius%20strip%20example.jpg "A Möbius strip or band")


As a more complex project, I individually designed and laser cut a Möbius strip out of a rigid plywood plank. A Möbius band is a weird topological object that contains only a single side. This can be tested by running your finger along the Möbius strip and eventually ending up where you started without having crossed a ridge (as opposed to a sheet of paper).



While browsing the web for inspiration, I fell on [a wooden Möbius strip from Jeremy Baumberg](https://obrary.com/products/moebius-strip). I downloaded the freely available template and converted the dxf file to svg using [an online converter called convertio](https://convertio.co/dxf-svg/). As it's a pretty big piece, decided to first make a smaller test piece to optimize the cutting parameters:

!['Mini Möbius svg used for testing purposes'](../fabzero-modules/images/mobius%20test%20svg.jpg "Svg used to test Möbius properties and parameters")

Colors are for visualisation purposes only, all lines were cut out on the Epilog laser cutter at speed 30% and power 100% for 4 passes. I based my initial guesses for the parameters on a test board very much resembling the one I used. I obtained my piece of plywood from [Emma](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/emma.dubois/) who did the effort of going to a convience store and sold me her leftover material, thank you Emma!


!['Image Möbius test'](../fabzero-modules/images/mobius%20test.jpg "Result of Möbius test")

As by magic, this cut pattern makes a rigid piece of plywood flexible, as every tiny beam can rotate along the next one a little bit. It looks and feels really weird when you hold it. It reminded me of [the wooden snake toys I used to play with as a kid](https://duckduckgo.com/?q=wooden+snake+toy&t=chromentp&iax=images&ia=images).

I also saw off a corner to see if the male and female puzzle pieces could be intertwined, which was the case (although it was quite difficult).

I thus used the same parameters for the big Möbius band, for which you can see the svg file, some images and [a video of the result](https://youtube.com/shorts/rkaBfCilEkU).

!['Image Möbius svg'](../fabzero-modules/images/mobius%20svg.png "Möbius svg")

!['Image Möbius result'](../fabzero-modules/images/final%20moebius%20band.jpg "Mobius result")

## 2.5 Files used

- The microcontroller scripts can be found here: 
    - [YD-RP2040 built-in W2812 red-yellow-green light cycle.py](../microcontroller%20scripts/YD-RP2040%20built-in%20W2812%20red-yellow-green%20light%20cycle.py)
    - [YD-RP2040 temperature and humidity change measuring LED.py](../microcontroller%20scripts/YD-RP2040%20temperature%20and%20humidity%20change%20measuring%20LED.py)
    - [YD-RP2040 temperature and humidity measuring.py](../microcontroller%20scripts/YD-RP2040%20temperature%20and%20humidity%20measuring.py)

- The svg files used for laser cutting can be found here:
    - [Moebius_test.svg](../svg%20files/mobius%20test.svg)
    - [Moebius.svg](../svg%20files/moebius.svg)

## My starting skills and knowledge on the topics

- An introductory course on how to use Arduino microcontrollers, which I followed around 5 years ago.
- Advanced scripting skills and notions of a function, parameters, arguments, ...


## What I learned on the topics
- Different [communication protocols](https://en.wikipedia.org/wiki/Communication_protocol) exist for digital devices (like microcontrollers) to communicate to each other.
- Microcontrollers can be instructed in Python using the Micropython interpreter.
- Datasheets of sensors are a forest of information.
- The laser of a laser cutter needs to be focused in the z-axis, ideally in the middle of the height of your material. This can be done manually (by adjusting the nose of the laser) or some machines (like the Epilog) can do it automatically.
- Rigid materials can be turned flexible using the cool line pattern from the Möbius strip project.