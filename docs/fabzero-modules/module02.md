# Week 2 -- 3D Modelling with Computer-Aided Design  

Digital fabrication often starts with **modeling the part or machine we want to make on a computer, a process called computer-aided design (CAD)**. By designing digitally before printing or machining it, we ensure we don't waste resources figuring out the different parameters of our components (size, number of holes/parts, ...), we can try different designs more quickly and we can model their properties (f.e. its strength using structural analysis).

## 1. OpenSCAD

OpenSCAD is a free and open-source software for CAD. It works by **combining, modifying and transforming geometric identities (or primitives)**. The basic geometric shapes that can be used and the functions to combine them can be found on [this cheat sheet](https://openscad.org/cheatsheet/index.html).

It works by writing a script for the shape(s) you want to generate (see it as a recipe to make a certain figure), which is composed of different lines of text that always have to end with a semicolon (;). Typing the command `cylinder(h = 10, r = 4, center = true);` and rendering the script with F5 generates a cylinder [^1] with a height of 10 mm, a radius of 4 mm and which is centered at the origin.

[^1]: In reality it gives back a prism with a certain number of sides as an approximation of the cylinder. The number of sides can be increased to better represent a cylinder by attributing a bigger number to the special variable $fn (f.e. $fn = 100).

<figure markdown>
  !['Output of the command `cylinder(h = 10, r = 4, center = true);`'](../fabzero-modules/images/openSCAD1.png "Output of the command `cylinder(h = 10, r = 4, center = true);`"){width="1000px" align="center"}
</figure>

If we would want a second identical cylinder next to it in the y-axis, we can add the following line that builds the identical cylinder as previously but then translates (or moves) it with 8 mm along the y-axis:
`translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);`

<figure markdown>
  !['Output of the command `translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);`'](../fabzero-modules/images/openSCAD2.png "Output of the command `translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);`"){width="1000px"}
</figure>

Next we can combine these two cylinders in a single shape with the `hull(){*shapes to envelop*};` command, which envelops (mathematically: gives the convex hull) of the shapes it is given as an argument. Let's look at the result of 
```
hull(){
    cylinder(h=10, r=4, center=true);
    translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);
};
```

<figure markdown>
  !['Output of the command `hull(){
    cylinder(h=10, r=4, center=true);
    translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);
};`'](../fabzero-modules/images/openSCAD3.png "Output of the command `hull(){
    cylinder(h=10, r=4, center=true);
    translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);
};`"){width="1000px"}
</figure>

The command `difference(){*shapes from which to take the difference*}` takes all the spacial points from the first shape it is given and removes these from the second shape it is given. Let's extend our code and use this function to drill a hole in our piece:

```
difference(){
    hull(){

        cylinder(h=10, r=4, center=true);

        translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);
    
};

    cylinder(h=10, r=3, center=true);

};
```

<figure markdown>
  !['`difference(){hull(){cylinder(h=10, r=4, center=true);translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);};cylinder(h=10, r=3, center=true);};`'](../fabzero-modules/images/openSCAD4.png "Output of the command `difference(){hull(){cylinder(h=10, r=4, center=true);translate([0, 8, 0])cylinder(h = 10, r = 4, center = true);};cylinder(h=10, r=3, center=true);};`"){width="1000px"}
</figure>

These are only some of the possible things you can do, but you are probably feeling that this script can get quite long and quite messy for more complex parts. So for other people (including yourself in the future) to understand your script, a few conventions exist:

- **Comment your code** by adding some text after a double slash (//) (see earlier images).

- **Indent your code** (= add empty space with the tab key) to structure parts that belong together, parts that are parameters of an operator, ...

- **Parametrize your code** by defining an arbitrary string for each figure element (f.e. we call the height of the cylinder `cylinder_height`), defining the value of this parameter at the start of your script (f.e. `cylinder_height = 4`) and using this parameter as an argument for the function itself (f.e. `cylinder(h = cylinder_height`). This way, if we are using the height of our cylinder at multiple instances and we want to change our design, we only have to change the initial parameter.

- **Write modules** to easily reuse pieces of your script. You can build a module called drilled_doublecylinder(cylinder_height, cylinder_radius, hole_radius) and describe once how to make this shape (add two identical cylinders next to each other, perform the hull() operation and take the difference with a smaller cylinder), and then use it as a function to build multiple drilled double cylinders.

## 2. Design a FlexLink part with OpenSCAD

Flexlink is a collection of Lego-compatible parts to build compliant mechanisms, designed by the Compliant Mechanisms Research Group (CMR) of Brigham Young University. **Compliant mechanisms are mechanical devices that use a flexible part (as opposed to rigid joints) to transfer or transform motion**, [according to this same research group](https://www.compliantmechanisms.byu.edu/about-compliant-mechanisms). Some advantages cited are a reduced part count, leading to easier production processes and a reduced price, and a more precise motion resulting in better performances. you can find all their FlexLink parts [on their website](https://www.compliantmechanisms.byu.edu/flexlinks).

Hereafter, I will describe how I designed my proper version of one of CMR's FlexLink parts called the Cross-Axis Flexural pivot.

[Here is a video of the result](https://youtube.com/shorts/WibXUf7hRHY?feature=share). The part is defined by two types of parts:

- Two anchor parts (top and bottom part) with holes to anchor the piece in existing lego bricks (yellow on following image).

- Two beams connecting the two anchor parts and providing flexibility (red on following image).

Inspired by [an OpenSCAD module from Pauline Mackelbert](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/), I decided to write my own module to assemble a Cross-Axis Flexural Pivot with given dimensions. The result and its used dimensions are presented in the following image:

<figure markdown>
  !['Designed Cross-Axis Flexural Pivot with used dimensions'](../fabzero-modules/images/Dimensions.png "Designed Cross-Axis Flexural Pivot with used dimensions"){width="1000px"}
</figure>

The commented *.scad* code used to generate this object can be found further. Some comments:

- The distance between two anchor parts was defined as the length of a unit times the number of units (= number of wholes), as estimated from the video above.

- The length of the beam is calculated using the Pythagorean theorem on the right triangle formed:
$$ BEAM\_LENGTH = \sqrt{UNIT\_DISTANCE^2 + (UNIT\_WIDTH*UNITS)^2}$$

- The angle for the beams is calculated using the inverse trigonometric function for the tangent:
$$BEAM\_ANGLE = \arctan{(\frac{UNIT\_DISTANCE}{UNIT_WIDTH*UNITS - \frac{UNIT\_WIDTH}{UNITS}})}$$

- The flexlink_pivot module doesn't directly use the anchorpart module because the holes have to pierced only after the beams are added.

- The error parameter serves to make the height of the hole-piercing cylinder just a little bit taller than the main anchor part to circumvent the bad rendering of the part preview. I got this trick from [Nicolas De Coster](https://fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/index.html).

- The UNIT_NUMBER parameter is not used for the moment but is to remind me to make a version where you can place multiple (more than two) anchor pieces next to each other and link them with beams.

- The initial parameter selection was done using this image of the dimensions of lego pieces found on [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Lego_dimensions.svg).

<figure markdown>
  !['Image displaying dimensions of LEGO blocks'](../fabzero-modules/images/Lego_dimensions.svg.png "Inspiritation for initial parameter selection"){width="500px"}
</figure>


The OpenSCAD code written can be found below, as well as in the [cadfile map](..\cadfiles) with the generated STL.

```
//BYU Cross-Axis Flexural pivot Flexlink reverse engineering script (https://www.compliantmechanisms.byu.edu/flexlinks)

//Author: Victor De Pillecyn

//Last modified: 08/03/2023

//License: Creative Commons (CC) BY
/////////////////////////////////////////////////

//ANCHOR parameters
UNITS = 4;
UNIT_WIDTH = 7.8;
UNIT_HEIGHT = 9.6/2;
HOLE_RADIUS = 2.55;

//FLEXLINK_PIVOT parameters
UNIT_NUMBER = 2;
UNIT_DISTANCE = UNIT_WIDTH*UNITS;

//BEAM parameters
BEAM_WIDTH = UNIT_WIDTH/5;
BEAM_HEIGHT = UNIT_HEIGHT/4;
//Beam length calculated using Pythagorean theorem
BEAM_LENGTH = sqrt(UNIT_DISTANCE^2 + (UNIT_WIDTH*UNITS)^2);
//Beam angle calculated using the right-angled triangle tangens property 
BEAM_ANGLE = atan(UNIT_DISTANCE/(UNIT_WIDTH*UNITS-UNIT_WIDTH/UNITS));

// Other parameters
error=0.01;
$fn = 100;


// Code to generate two anchorparts
//anchorpart(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS);
//translate([0, UNIT_DISTANCE, 0])anchorpart(UNITS, UNIT_WIDTH, UNIT_HEIGHT, HOLE_RADIUS);

module anchorpart(units, unit_width, unit_height, hole_radius) { 
difference(){
    hull(){
        for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height, r=unit_width/2, center=true);
        }
    }
    for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height+error, r=hole_radius, center=true);
    }
}};


//Code to generate the crosssectional beams
//beampart(UNIT_WIDTH, UNIT_HEIGHT, UNIT_DISTANCE, BEAM_WIDTH, BEAM_HEIGHT, BEAM_LENGTH, BEAM_ANGLE);

module beampart(unit_width, unit_height, unit_distance, beam_width, beam_height, beam_length, beam_angle){
    color([1,0,0])union(){
        translate([-unit_width/2,0, unit_height/2])rotate([180, 0, beam_angle])cube([beam_length-1, beam_width, beam_height]);
        
    mirror([0,0,1])translate([-unit_width/4, unit_distance, unit_height/2])rotate([180, 0, -beam_angle])cube([beam_length-2, beam_width, beam_height]);
    }};

// If I generate just the beams and anchors separatily, the beams stick out in the holes of the anchors, I thus needed to generate the beams, union them with the anchors, and then perforate the anchors

//Code to generate a complete Flexlink pivot
flexlink_pivot(UNITS, UNIT_WIDTH, UNIT_HEIGHT, UNIT_DISTANCE, HOLE_RADIUS, BEAM_WIDTH, BEAM_HEIGHT, BEAM_LENGTH, BEAM_ANGLE);

module flexlink_pivot(units, unit_width, unit_height, unit_distance, hole_radius, beam_width, beam_height, beam_length, beam_angle){
    difference(){
            union(){
                beampart(UNIT_WIDTH, UNIT_HEIGHT, UNIT_DISTANCE, BEAM_WIDTH, BEAM_HEIGHT, BEAM_LENGTH, BEAM_ANGLE);
                hull(){
                    for (i = [0:units-1]){
                        translate([unit_width*i, 0, 0])cylinder(h=unit_height, r=unit_width/2, center=true);
                    }
                }
                hull(){
                    for (i = [0:units-1]){
                        translate([unit_width*i, UNIT_DISTANCE, 0])cylinder(h=unit_height, r=unit_width/2, center=true);
                    }
                }
            }
        for (i = [0:units-1]){
            translate([unit_width*i, 0, 0])cylinder(h=unit_height+error, r=hole_radius, center=true);
        for (i = [0:units-1]){
            translate([unit_width*i, UNIT_DISTANCE, 0])cylinder(h=unit_height+error, r=hole_radius, center=true);
      }}}}
```

## 3. Force analysis with FEM in Freescad

To get an idea of where the weak spots in the part would be upon bending it, I performed a quick initial structural analysis using the finite element method (FEM) in Freescad. I thereby followed [this youtube tutorial](https://www.youtube.com/watch?v=qWtMJagDoRk) and obtained the following result:

<figure markdown>
  !['Stress analysis upon bending of Flexlink part'](../fabzero-modules/images/FEM.png "Weak regions upon bending the flexlink part"){width="500px"}
</figure>

The bottom anchor part is fixed in place, and a force is applied in all holes to the right (indicated by the red arrows). We observe, (maybe unsurprisingly) that the flexible beams are the weakest part of the structure when applying this force, and enough time should be spent to reinforce this without compromising flexibility.



## My starting skills and knowledge on the topic

- Euclidean geometry and its most famous trigonometric ratios (Pythagorean, (cos)sinus/tangent)
- Advanced scripting skills and notions of a function, parameters, arguments, ...

## What I learned on the topic
- Designing a complex part in OpenSCAD (using functions: cube(), cylinder(), union(), hull(), difference(), translate(), rotate(), mirror(), color(), module(), $fn parameter) and exporting it in .stl format.
- Slicing part and adding supports with PrusaSlicer v2.5
- Creative Commons code licenses
