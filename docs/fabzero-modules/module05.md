# Week 5 -- Group dynamics and final project

In this chapter, I will:

1. Summarize some things I learned about **how to improve group dynamics** (both from the course but also from my own experience as a chief scout leader).

2. Present my group's **Final project**.


## 1. Improving teamwork

As a basketball player and a scout, I've always been a fan of working in a group. I truly believe that if done properly, **teamwork reduces everyone's required effort and multiplies the quality of the outcome**.

The catch being that it has to be done properly, we saw some tricks on how to improve the way you work as a team (= group dynamics).

### 1.1 Getting everyone in the group to talk

In my experience, **teamwork makes coming up with ideas much easier**, as people often have different expertise/life experiences and thus can build and improve on each other's ideas.

But for this to work, **everyone needs to feel comfortable to talk and share their ideas with the group**. This starts with having a safe group climate, where people don't feel like they will be deprecated when expressing an idea perceived as bad by the rest of the team members. Always **be thankful and appreciative of ideas** being added by someone, even if you personally think the idea itself is bad.

But even if people are comfortable sharing their information, some people are intrinsically more introverted and will be less likely to share their vision of the situation. Therefore, some tricks exist to obtain similar speaking time between group members:

1. **Go around the table** (not literally): Pick someone who starts expressing information/ideas, and then go to the next person until you have heard from everyone. It sounds stupid, but we often do this in scout meetings, and it works, as some people don't participate in the meeting because they have difficulty joining an ongoing discussion between very active speakers. Everyone being quiet and expecting an answer from you can motivate you to express yourself.

2. **Keep track of speaking time, display and optionally limit it**: Seeing who has talked a lot and who hasn't can implicitly balance things out, but explicitly limiting the speaker time per person is even stronger.

3. **Introduce anonymity**: Give the possibility to each team member to express their ideas anonymously, especially with subjects that are difficult to talk about or involve personal relationships.

4. **Give people time to think**: While some people are quick to make up their minds and speak out, other people need to process the information they (just) received before forming an opinion or coming up with ideas.

*Sources: Introductory course on group dynamics, own experience and [this youtube video from Kara Ronin](https://www.youtube.com/watch?v=vfYYIrj445k)* 

### 1.2 Group decision-making

While **teamwork makes coming up with ideas much easier, it makes finally deciding what option to pick much harder**, as people can differ in what arguments they find most convincing. 

**Firstly (and often forgotten), the question arises if a decision has to be made at this time**. If the members of the group do not have enough information or a strong enough will to make a decision, it can be postponed or canceled.

**Secondly, the power to decide has to be attributed (if it isn't implicitly): to a certain individual, a subgroup or the group as a whole.** Arguments to make decisions with a reduced number of people may be that choices can be made more quickly, or that some people in the group have advantageous expertise or predefined authority (and thus responsibility). The more people involved in a decision, the more collaborative (vs. independent) it is, and possibly the more egalitarian (vs. hierarchical) and transparent (vs. private) it is.

**Lastly, in the case that multiple people have to make a decision, different ways of group decision-making are available**, presented in the following table (which is somewhat too extensive) and shortly discussed below.

!['Image of a table presenting when to pick which decision-making strategy](../fabzero-modules/images/Decisionmakingtypes.png "Table presenting when to pick which decision making strategy")


1. **Autocratic**: One person decides, without input from others. See possible arguments for this method earlier.

2. **Avoidance**: You decide not to decide (yet). See possible reasons for this method earlier.

3. **Consensus**: The group changes the decision until everyone is reasonably satisfied, and a compromise is found.

4. **Consent**: Out of multiple pre-determined options, the group picks the one that doesn't have any objections from group members (is not necessarily the same option that most people prefer)

5. **Consultative**: One person decides, with input from others.

6. **Delegate**: The group decides on a member that will determine the final option, because this person has the best information for instance.

7. **Democratic**: Everyone votes to determine the outcome. This can be a simple majority vote (one vote per person, option with the most votes wins) or a multiple vote (multiple votes per person, option with the most votes wins).

8. **Stochastic**: Pick a random option between multiple viable ones.


*Sources: Introductory course on group dynamics, own experience, [this article from Kavana Tree Bressen](https://effectivecollective.net/library/consensus-in-sharing-law.pdf) and [the website of The Decider app](https://thedecider.app/about).* 


## 2. Final project: Prototyping a solution towards a UN Sustainable Development Goal

![Group picture](images/group%20photo%20annotated.png)

![project banner](images/group10-slide.png)

## 2.1 How did we become a group?

Everyone in the class was asked to **bring an object that represents an issue that they find interesting**. I decided to bring a reusable fabric tissue, representing the issue that a lot of the objects we use today are ephemeral (= used for a very short amount of time). When asked to look for people with objects representing similar problems, our group members assembled as they had pieces of trash as an object and **we all had a similar theme in mind: Waste**.

While introducing ourselves, we quickly learned that by chance, all of us are basketball players! We also came up with a starting observation: That waste exists because our current society is characterized by a linear use of resources. We extract them, manufacture them into usable objects, and throw them away as trash (= physical objects that don't have a function in our society anymore). **We hereby broadened our theme to be about the linear use of resources**.

## 2.2 Listing up the problems within our chosen theme: The linear use of resources

While brainstorming, we came up with **the following (world) problems** that have to do with our starting observation:

- **We need a lot of raw materials** to make our everyday objects, and we often destroy ecosystems to obtain them.
- The raw materials we need **come from everywhere around the world** and thus have to travel far, which takes time and emits greenhouse gasses from transport.
- We **use certain objects for a (very) short period of time** (packaging, single-use, ...), thereby quickly generating trash.
- Objects we use every day **are sometimes purposely designed/programmed to fail more easily**, so consumers buy new products regularly, thereby again generating trash more quickly.
- **A lot of the world's trash ends up in landfills**, thereby taking up land that could be used for something else.
- **Some of the world's trash ends up in different ecosystems**, thereby hindering the organisms that live there (f.e. rivers and oceans).


## 2.3 Choosing a more specific problem

After discussing the previously mentioned issues related to the linear use of resources, we decided to pick the one we felt most passionate about. This issue would then be studied in more detail to find a potential solution.

Following the consensus method, we combined two problems and finally settled on **fixing the overconsumption of primary resources by reusing waste**. This was deemed too broad by the professor, who asked us to pick a hard constraint and add it to our problem statement.

We decided to **focus on a single type of waste and brainstormed again** to come up with different waste types we would like to focus on: Aluminium cans, ballpoint pens, used pizza boxes, coffee grounds, single-use plastics & old shoes.

From this list, using a multiple vote, **our group picked the final and specific problem: The excess of waste due to coffee overconsumption.**

To identify the root causes and consequences of this issue, **we created a problem and a solution tree**, [as explained in this video from the European Comission](https://www.youtube.com/watch?v=9KIlK61RInY).

!['Image of the problem and solution trees for our chosen problem](images/arbre.jpg "Problem and solution trees for our chosen problem")

## 2.4 Choosing a solution to the problem

To tackle the problem of excess waste due to coffee overconsumption, we identified what root causes we could tackle. **A lot of the root causes we came up with are structural societal problems that require government regulations to change (change the economic system, tax primary resources, ...)**.

We came up with the following three practical solutions that we could implement in the Fablab:

- **Construct a device to reheat your coffee when it gets cold**: If fewer people make a second coffee because their first one turned cold, less coffee waste is generated. Unfortunately, we estimate chances are low that people are okay with drinking a warmed-up coffee, and not everyone's cup will be suitable to heat up.

- **Build a caffeine measuring tool to make people aware of their caffeine intake**: If you know the daily amount of caffeine you are taking in, that can be the first step to reducing it, thereby lowering the amount of coffee waste you generate. The doubt with this project is that addiction is a complicated topic to tackle, and the link with coffee-related waste is not obvious.

- **Develop a coffee capsule from agar agar**: This would retain the ease of use and the implicit dosage a lot of capsule users like, but without generating aluminum waste. The problems with this idea are that it already exists on a commercial level and that the capsules require a new type of coffee machine ([coffeeB](https://www.coffeeb.com/en-ch/)).


Finally, we chose the caffeine measuring tool, as it seemed to be a novel idea and doable in the FABLAB. We made a working prototype illustrating the concept in the following picture.

![Image of the prototype for a caffeine measurement tool](../fabzero-modules/images/PROTOTYPE%20FINAL.png)

# 2.5 State of the art

The state of the art is presented under [this chapter on our group website](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-10/Caff%C3%A9ineCheck/)

# 2.6 The build process

# 2.6.1 General overview

First, we sketched how the caffeine meter would work on paper, based on the information we acquired. We converted this sketch into the following 3D model using OpenSCAD.

![](images/first%20model.png)

The object contains the following parts:

1. A funnel to pour coffee into (yellow), made with a 3D printer.

2. A see-through cuvette (light blue) in a cuvette support (blue-green-red). It is obtained from a lab that has a lab-grade spectrophotometer.

3. A UV emitter and sensor (not on the model), placed on both sides of the cuvette and controlled by a microprocessor.

4. Everything is put inside a wooden box (transparent pink). The box is cut using a laser cutter and engraved with information about the waste generation of coffee consumption, as well as the medical effects of coffee overconsumption.

## 2.6.2 How it works

All matter absorbs and reflects light. Light is a wave that is characterized by its wavelength (measured in nanometers, nm): low-energy waves like radio waves have a very big wavelength, and high-energy waves like X-rays have a very small wavelength. Our eyes only perceive light with wavelengths between 400 nm and 700 nm, called visible light. 

![](images/light%20spectrum.png)

Every chemical molecule has a unique absorption spectrum, meaning the molecule doesn't absorb all wavelengths equally well. **The molecule caffeine for instance, doesn't absorb visible light, but it does absorb the light with wavelengths representing UV light** ([this video shows the measurement of the absorption spectrum of some dyes and a caffeine solution](https://www.youtube.com/watch?v=q_JBq1kdev8)). The following image shows the absorption spectrum of pure caffeine, calculated with a lab-grade spectrophotometer that sends the different wavelengths one by one through the solution and measures how much is absorbed.

![](images/caffeine%20spectrum.png)

We can use this feat to calculate the concentration of caffeine in a coffee: **The more a coffee has caffeine, the more it absorbs UV light**. We thus want to send light with a wavelength of around 274 nm through a coffee sample and measure how much is absorbed. The absorbance can be calculated as follows:

![](images/cuvette%20workings.png)

Absorbance A = log10 (I0/I1)

With I0 being the intensity of the light before it passes through the cuvette and I1 after.

The [Beer–Lambert law](https://pubs.acs.org/doi/10.1021/ed039p333) gives us a **relationship between the absorbance and the concentration of absorbing compounds**:

Absorbance A = c * ε * l

With:

c = molar concentration (mol/L)

ε = molar absorption coefficient (cm^-1M^-1)

l = optical path length (cm)

The molar concentration (how many molecules of coffee per unit of volume) is what we want to know! The molar absorption coefficient ε is a measurement of how strongly a chemical species absorbs a specific wavelength and the optical path length is the distance traveled by the light waves.

**The concentration of caffeine in a coffee can thus be measured using the following formula:**

Concentration c = log10 (I0/I1) * ε * l

As the concentration of caffeine is not the sexiest metric for everyone to understand, we can convert a concentration to mass by using the volume of the coffee:

Mass m = M * c * V

With:

M = molar mass of caffeine (g/mol)
c = molar concentration (mol/L)
V = volume (L)

The mass is in grams (g) and can be converted into milligrams (mg) by multiplying by 1000.

# 2.6.3 Building the CaffeineCheck

## 2.6.3.1 UV emitter and UV sensor

Hardware requirements:

- 1 YD-RP2040 microcontroller
(available [on Alibaba for 4,86 €](https://fr.aliexpress.com/item/1005005576423569.html?spm=a2g0o.productlist.main.15.44b37ec2zktz6k&algo_pvid=d7305585-c263-4995-8771-6e000aa4e0cb&algo_exp_id=d7305585-c263-4995-8771-6e000aa4e0cb-7&pdp_npi=3%40dis%21EUR%2111.58%214.86%21%21%21%21%21%40211bf3f116843243526463836d080f%2112000033612766418%21sea%21BE%214572784929&curPageLogUid=PFPraR3bbfYv))
- 1 UV Light Sensor Module (200-370nm)
(available [on Tinytronics for 5 €](https://www.tinytronics.nl/shop/en/sensors/optical/light-and-color/uv-light-sensor-module-200-370nm))
- 1 UV Emitter (UV-C, ideally 273 nm wavelength)
(available [on Farnell for 4,82 €](https://be.farnell.com/vishay/vlmu35cb21-275-120/uv-emitter-273nm-uv-c-3-45mm-x/dp/3873085))
- A 6-volt battery, ideally controlled by a switch.
- Optional: an LCD screen to display the results.
- Cables to connect everything

Assembly (**UNPOWER THE MICROCONTROLLER FOR ASSEMBLY**):


1. UV SENSOR: Use the cables to connect the GND pin of the UV sensor to the GND pin of the microcontroller, the UV sensor VCC pin to the microcontroller 3V3 pin and the UV sensor OUT pin to the microcontroller pin number 26.


2. UV EMITTER: **IMPORTANT WARNING: UV-C light is very harmful to your eyes and can mutate the DNA in your cells. YOU SHOULD NEVER CONNECT THE UV EMITTER TO A POWER SOURCE WITHOUT PROPER EYE AND SKIN PROTECTION** Carefully solder a red and a black cable to the anode (positive side) and cathode of the UV-C emitter, respectively. Connect them to the correct sides of the battery.


Software requirements:

- [This is the script used to run the hardware without LCD screen](../fabzero-modules/files/analog_uv_sensor/SCRIPT%20CAFFEINATOR.py)

- [This is the script used to run the hardware with LCD screen](../fabzero-modules/files/analog_uv_sensor/SCRIPT%20CAFFEINATOR%20LCD.py)


The script first imports the required packages and defines on which pins the different components are (addressable LED on pin 23 and UV sensor on pin 26). Then it initiates the used variables:

- Optical path length, or the distance traveled by the UV light: OPL = 1 cm
- Molar absorption coefficient of caffeine in coffee: MAC = 386 cm^-1M^-1
_ Molar mass of caffeine: MM_CAF = 194.19 g/mol

- The volumes of standard coffees to convert from concentration to mass:
VOLUME_ESPRESSO = 0.059 L, VOLUME_CAPPUCCINO = 0.177 L, VOLUME_REGULAR = 0.25, VOLUME_LARGE = 0.35

- The number of blanks to measure from which to take the average (NR_BLANKS, NR_MEASUREMENTS) as well as the frequency of measurement (BLANKS_PER_SEC, MEASUREMENTS_PER_SEC)

The intensity of the light which passes through the liquid is converted by our sensor to a voltage. Our sensor has a baseline voltage that it measures, even when no UV light is shined upon it. This baseline voltage (V0) has to be subtracted from our measurements giving rise to the V0 in our formula:

Concentration c = log10 ((I0-V0)/(I1-V0)) * ε * l

## 2.6.3.2 The cuvette support

To hold the cuvette, the UV sensor module and the UV emitter in place, we designed and 3D-printed a support piece.

It is made up of a base to be solidly placed on the ground, a holder for the cuvette and walls on either side that inhibit UV light from going around the cuvette. In the second version (see image), we added a support to keep the UV emitter and sensor in place.

![The cuvette support front and side view](images/cuvette%20support%20full.png)

## 2.6.3.3 The box

The box holding all the components is made out of a 3mm thick mdf plywood plank that is engraved and cut with a laser cutter. The svg file for the BasedBox was downloaded from [boxes.py](https://festi.info/boxes.py/) and drawings and text to be engraved were added manually. The first box we made had dimensions of 100x70x100 mm. The following parameters were used for cutting on the EPILOGUE laser cutter: speed = 30%, power = 100% and frequency = 100% for 4 passes. For engraving, the following parameters were used: speed = 30%, power = 100% for 2 passes.

![box v1 overview](images/box%20v1%20overview.png)

The first box can be seen [in this video](https://youtube.com/shorts/Cief4kHxgSY). While this was a great first result, the box was too small to fit all the components in (cuvette holder + electronics), and the QR code was not readable because it was not well aligned with the box when engraved.

The second version of the box was bigger (150x100x100 mm) and we added a window for an LCD screen.
Here are the SVG files for [the **complete** bigger box](../fabzero-modules/files/Boitier/box%2015x10%20decorated%20with%20LCD%20screen%20SIDE.svg), [only the lines to be cut](../fabzero-modules/files/Boitier/Bigbox%20v2%20CUT.svg) and [only the drawings to be engraved](../fabzero-modules/files/Boitier/Bigbox%20v2%20ENGRAVE.svg).

![box v2](images/box%20v2.png)

# 2.6.4 Using the CaffeineCheck

To use the CaffeineCheck, you **first have to insert a quartz cuvette filled with a liquid used as blank to measure the absorbance of the UV light without caffeine**. In our first attempts, we used water as blank, until someone in our class (I can't remember his name) suggested we **use decaffeinated coffee as a blank**. Indeed, the difference in absorbance of UV light between a decaffeinated coffee and a regular coffee can largely be attributed to the presence of caffeine molecules*. 

**Afterwards, you can insert a quartz cuvette filled with a coffee** and measure the absorbance again. With the formulas illustrated earlier you can convert this difference in absorption to a concentration of caffeine!

**Does it work?** 

The machine detects a smaller amount of caffeine in water (almost zero), compared to a grenadine solution (which also has UV absorbing molecules), which in turn has less than in a decaffeinated coffee, followed by a regular coffee. **We can thus state our machine works, at least qualitatively. Additionally, the concentrations of caffeine measured in a regular coffee are realistic and close to what we would expect** (0.5-4 g/L, [Olechno et al., 2021](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8228209/)).


*Other molecules also absorb UV light, but are (hopefully) not removed during the decaffeinating of coffee.

# 2.6.5 Measuring accuracy and precision of the CaffeineCheck

To assess the accuracy/precision of the CaffeineCheck, we figured we could **prepare coffees with a known and realistic concentration of caffeine and compare it to the measurement of our device**, in what is known as a calibration curve.

To prepare the known caffeine concentrations, we went to a local pharmacy and asked for a bit of pure caffeine (they gave it for free to support the project!). In the lab, using a pipet and an analytical scale we prepared the following concentrations. To be more precise, we prepared a dilution series, so we prepared one concentration and then diluted it to obtain others.

![Used concentrations for calibration curve](images/Calibration%20concentrations.png)

The dilutions were calculated using the formula c1 * V1 = c2 * V2.

**Results**

Unfortunately, we discovered the main drawback of our prototype: it can not handle too caffeinated liquids. We did all this work, to realize that when the coffee put inside the cuvette is too caffeinated, our machine measures no light coming through (as if the light is not on), not giving us a different absorption falsifying our results. We should retry with smaller concentrations or add more UV emitters to increase the base signal.

![Results of failed calibration](images/calibration%20results.png)

## My starting skills and knowledge on the topic
- Extensive experience in group meetings with various people on a wide spectrum of topics.
- A lot of group decision-making experience, being in schoolwork groups or scouting groups.
- Experience in choosing and leading different group decision-making processes as a chief scout leader.

Extensive experience in group meetings with various people on a wide spectrum of topics.
A lot of group decision-making experience, being in schoolwork groups or scouting groups.
Experience in choosing and leading different group decision-making processes as a chief scout leader.
## What I learned on the topic
- Choosing not to decide can be a good option.
- [The DeciderApp](https://thedecider.app/), a website that asks a few yes/no questions to help you determine the best group decision-making method
- [The website of lejugementmajoritaire](https://mieuxvoter.fr/le-jugement-majoritaire), an online platform to perform multiple voting easily.
- I learned more than I could have imagined about coffee, spectroscopy, integration of different components made in the Fablab and working as a team!


