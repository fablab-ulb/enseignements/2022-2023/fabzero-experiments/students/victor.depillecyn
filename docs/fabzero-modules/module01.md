# Week 1 -- Documenting and project managing

While taking my first steps in the intricate world of digital fabrication with the ULB university course [How To Make (almost) Any Experiments](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/), I will be documenting my work in this blog for two reasons:  

- For other people to potentially find answers to identical problems.  

- For myself to be able to reread and remember how I solved a specific problem.

In this first week, I describe how I am documenting this journey, what issues and solutions I have encountered and what elements I have learned.

## 1. How this project was documented

### 1.1 The website

You, the reader, are (probably) reading this on a website hosted by the Fablab of the ULB university. The layout and content of my personal page on this website are described in HTML language, but this can be cumbersome to learn. The website is thus written in the more simple Markdown language and then converted into HTML format by MKDocs. The different files describing the website are stored in the GitLab space of the ULB Fablab (see Figure [1](#gitlabdocks)) and the different versions of these files are tracked using the version control system git.


<figure markdown>
  !['An image of my face not smiling'](../fabzero-modules/images/GitLab_dockx.png "Screenshot of the documents that describe website content and layout."){width="500px" align="center"}
</figure>
<figcaption align = "center">Screenshot of the documents that describe website content and layout.</figcaption>



This is what the different files are for:

- **docs**: This folder contains the content of the website. Each website page has a file in markdown format (.md) wherein you can modify the page's text. It also contains an 'images' folder wherein the images can be stored to be referenced in the text (See [The Markdown language](#markdownlanguage)).  
- **.gitignore**: This file lists the files you want the version control system git to ignore when you commit (more on this later).   
- **.gitlab-ci.yml**: The configuration file for the GitLab CI/CD (Continuous integration/continuous delivery).  
- **README.md**: A description of the repository, how it has to be used and what files play what role (like this very explanation).  
- **mkdocs.yml**: This file describes MKDocs the general parameters of the HTML generated.  
- **requirements.txt**: List of the packages that MKDocs downloads before converting de Markdown pages into HTML format.

### 1.2 The Markdown language

The information contained in a text can be transferred to the reader by choosing a good sequence of words. Authors are praised for their ability to write good texts by choosing the right semantic elements. But a text can be made more clear by improving it graphically, using formatted text. Words can be written in **bold** or *italic*, made <font size="5"> bigger </font> or put in a (numbered) list or a table. Markdown is a language that facilitates this in a human-readable way, by adding characters to the text. An overview of the markdown syntax can be found [here](https://www.markdownguide.org/cheat-sheet/). It contains fewer *graphical functionalities* than the HTML language, but HTML tags can be used in a Markdown file to complement this.

#### 1.2.1 Add images in Markdown text

Images can be added in two ways, one proper *Markdown-like* way or via HTML tags:

- Markdown notation to add an image:  
```
![alt text](pathtoimage/imagename.JPG "Mouse-over text")
```


  *Note*: 'alt text' is a description of the image that can be read out or visually impaired people.

- The HTML-like tag notation to add an image:

```
<figure markdown>
  !['alt text'](images/pathtoimage/imagename.JPG "mouse-over text")
</figure>
<figcaption>Caption that you want bellow the image
</figcaption>
```
The advantage of this method is, that plenty of extra features can be added and changed with the HTML tag: resizing the image, aligning it in a certain way on the page or adding an image caption. This is done by adding arguments in the HTML tag as follows (plenty of other options can be added):
```
<figure markdown>
  !['alt text'](images/pathtoimage/imagename.JPG "mouse-over text"){width="*number*px" align="right"}
</figure>
<figcaption align = "center">Caption of image.
</figcaption>
```

Personally coming from the LaTeX environment, I would have liked a way to automatically number my figures, the following piece of code at the start of the page worked for a moment but doesn't seem to anymore for some reason (which I got from [this web article](https://tamarisk.it/automatically-numbering-figures-in-markdown/)):


```
<style>
    
    body { counter-reset: figureCounter; }
    
    figure { counter-increment: figureCounter; }
    
    figure figcaption:before {
        content: "Fig " counter(figureCounter) ": "
    }
</style>
```

Every `<figcaption>` should be prepended with a fig number that is associated with its location, even if you don't caption one of them.


#### 1.2.2 Add links and references to Markdown text

Links to refer to websites or images in the document can be added like this:  

`[text to display](image location OR website URL)`  

In this way, I can refer to the [figure you saw earlier](../fabzero-modules/images/GitLab_dockx.png) or to [the homepage of my website](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/victor.depillecyn/).


#### 1.2.3 Add videos to Markdown text

There seems to be no **Markdown-ish** way of embedding videos, so I tried the following HTML tag which I got from [the FabZero website of Pauline Mackelbert](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module01/).

```
<video controls muted>
<source src="../../images/module/video.mp4" type="video/mp4">
</video>
```

Unfortunately, this didn't work. Just treating the video as an image also did not work. I decided to upload my videos to Youtube and link to [them this way](https://youtu.be/jMf17nQdtcg).


### 1.3 Locally adapting the website

The content of the website can be changed directly through Gitlabs Web IDE. A more flexible, offline and decentralized way of working is cloning the repository from Gitlab onto your PC and working in a local environment, regularly committing changes through the git version control system and sporadically pushing it back up to Gitlab.

To ensure the connection between Gitlab and my local version of the project, I installed the Gitlab Workflow extension on my Visual Studio code IDE and connected it to my account (which automatically generates and shares an SSH key). When in the Gitlab workflow tab, calling the command palette (Ctrl+Shift+P) gave me the option to choose which Gitlab repositories to clone locally.

[This 16 seconds Youtube video](https://www.youtube.com/watch?v=p4GTVx_Nd2s&t=3s) seems to approximatively summarise what I have done once the Gitlab Workflow extension was downloaded.

I also use the Visual Studio code extension Grammarly for suggested grammar corrections.

### 1.4 Adapting image size

The idea is to keep the image and video sizes to a minimum. To do this, all images were cropped as much as possible. If an image is over 500 kB, I cropped it using the Windows image cropping tool to reduce its file size to under 500 kB. This was deemed much quicker then downloading specific software, as it is only a few images that need cutting.

## 2. Project managing

While I feel like I have strong organization skills, I also know I tend to work on projects whenever I have a bit of time, and I also tend to make my projects very detailed and generalizable immediately.

For this journey, I will thus focus on two of the project-managing principles illustrated in class:

1. Parkinsow's law: Work expands to fill the time available for its completion. I will tackle this by pre-defining on Sunday what moments I will be working on this course for the coming week.

2. Spiral development: Complete the bare minimum version of the project, then incrementally improve it with added features and refinements. Doing the bare minimum will be difficult for me, but I will be attentive to write down any improvements I want to make instead of making version 1.0 very advanced.

## My starting skills and knowledge on the topic

- Advanced programming skills and using an IDE (Visual Studio Code).
- Advanced documenting skills as a 7th-year student having finished numerous reports and a Master's thesis. In the latest years, this was often done in the LaTeX format in the Overleaf editor.
- Basic experience with using Markdown and git.

## What I learned on the topic
- Different ways of adding images in Markdown.
- Automatically number, caption and align images in Markdown.
- Use footnotes in Markdown.
- Some basic principles of project managing.
