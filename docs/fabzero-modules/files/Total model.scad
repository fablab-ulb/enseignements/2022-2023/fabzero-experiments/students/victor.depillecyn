//https://forum.openscad.org/Best-way-to-draw-a-funnel-td9741.html


//PARAMETRES ENTONOIR
BOWL = 30;
BOWL_R = 30;
STEM = 40;
STEM_R = 1; // Cuvette largeur 1.25
NECK_R = 1; 
THICKNESS = 1;
CURVE_R = 10;
PLACEMENT_ENTONOIR_Z = 80;

//DIAMETRE TROU COUVERCLE = ? cm


//PARAMETRES BOITE
BOITE_X = 100;
BOITE_Y = 70;
BOITE_Z = 100;

$fa = 5;
$fs = 0.1;
eps = 0.01;


// PARAMETRES FAKECUVETTE
CUVETTE_X = 12.5;
CUVETTE_Y = 12.5;
CUVETTE_Z = 45;


// PARAMETRES BASE
BASE_XY = 50;
BASE_Z = 3;

//PARAMETRES SUPPORTS
SUPPORT_HEIGHT = 5;
SUPPORT_WIDTH = 2;
SUPPORT_LENGTH = CUVETTE_X + SUPPORT_WIDTH*2;

//PARAMETRES PAROIS
PAROIS_HEIGHT = 50;
PAROIS_WIDTH = BOITE_Y/2-CUVETTE_Y/2-SUPPORT_WIDTH/2;
PAROIS_THICKNESS = 1;

//PARAMETRES CROWSNEST
CROWSNEST_HEIGHT = 30;
CROWSNEST_WIDTH = 7;
CROWSNEST_LENGTH = BASE_XY/2;
CROWSNESTHOLE_DEPTH = 20;
CROWSNESTHOLE_WIDTH = 4;
CROWSNESTHOLE_LENGTH = 17;

crowsnest();
parois();
translate([0, 0, PLACEMENT_ENTONOIR_Z-STEM])entonoir();
fakecuvette();
base();
cuvette_supports();
translate([0, 0, BOITE_Z/2])color([0.5,0,0.5], alpha = 0.2)boite();


module fakecuvette(){
    color([0,0.5,0.5], alpha = 0.2)translate([0, 0, CUVETTE_Z/2+BASE_Z])cube([CUVETTE_X, CUVETTE_Y, CUVETTE_Z], center = true);
    }
    



module base(){
    color([0.4,0.4,1])translate([0, 0, BASE_Z/2])cube([BASE_XY, BASE_XY, BASE_Z], center = true);

    }
    
module cuvette_supports(){
    //SUPPORT 1
    color([0.4,1,0.4])translate([0, CUVETTE_Y/2+SUPPORT_WIDTH/2, SUPPORT_HEIGHT/2+BASE_Z])cube([SUPPORT_LENGTH, SUPPORT_WIDTH, SUPPORT_HEIGHT], center = true);
    //SUPPORT 2 (opposes SUPPORT 1)
    mirror([0,1,0])color([0.4,1,0.4])translate([0, CUVETTE_Y/2+SUPPORT_WIDTH/2, SUPPORT_HEIGHT/2+BASE_Z])cube([SUPPORT_LENGTH, SUPPORT_WIDTH, SUPPORT_HEIGHT], center = true);
    //SUPPORT 3
    color([0.4,1,0.4])translate([CUVETTE_X/2+SUPPORT_WIDTH/2, 0, SUPPORT_HEIGHT/2+BASE_Z])rotate([0,0,90])cube([SUPPORT_LENGTH, SUPPORT_WIDTH, SUPPORT_HEIGHT], center = true);
    // SUPPORT 4 (opposes SUPPORT 3)
    mirror([1,0 ,0])color([0.4,1,0.4])translate([CUVETTE_X/2+SUPPORT_WIDTH/2, 0, SUPPORT_HEIGHT/2+BASE_Z])rotate([0,0,90])cube([SUPPORT_LENGTH, SUPPORT_WIDTH, SUPPORT_HEIGHT], center = true);
    }


    

module profile() {
        hull() {
                translate([STEM_R, 0])
                        square([THICKNESS, eps]);

                translate([NECK_R + CURVE_R, STEM])
                        circle(r = CURVE_R);
               
                translate([BOWL_R, STEM + BOWL - eps])
                        square([THICKNESS, eps]);
        }
}

module entonoir() {
    rotate_extrude()
        difference() {
                profile();
                translate([THICKNESS, 0])
                        profile();
        }
    }
    
    
module boite(){
    cube([BOITE_X, BOITE_Y, BOITE_Z], center = true);
   
    }
    
module parois(){
    //PAROIS 1
    color([1,0,0.2])translate([0, PAROIS_WIDTH/2+CUVETTE_Y/2+SUPPORT_WIDTH/2, PAROIS_HEIGHT/2])cube([PAROIS_THICKNESS, PAROIS_WIDTH, PAROIS_HEIGHT], center = true);
    //PAROIS 2 (opposes PAROIS 1)
    color([1,0,0.2])translate([0, -PAROIS_WIDTH/2-CUVETTE_Y/2-SUPPORT_WIDTH/2, PAROIS_HEIGHT/2])cube([PAROIS_THICKNESS, PAROIS_WIDTH, PAROIS_HEIGHT], center = true);   
    }
    
module crowsnest(){
    // FOR UV EMITTER
    color([1,0,1])translate([BASE_XY/4, 0, CROWSNEST_HEIGHT/2])difference(){
        difference(){
            cube([CROWSNEST_WIDTH, CROWSNEST_LENGTH, CROWSNEST_HEIGHT], center = true);
            translate([0, 0, CROWSNEST_HEIGHT-CROWSNESTHOLE_DEPTH])cube([CROWSNESTHOLE_WIDTH, CROWSNESTHOLE_LENGTH, CROWSNESTHOLE_DEPTH], center = true);
        translate([-CROWSNEST_WIDTH/4, 0, CROWSNEST_HEIGHT-CROWSNESTHOLE_DEPTH])cube([CROWSNEST_WIDTH-CROWSNESTHOLE_WIDTH/2, CROWSNESTHOLE_LENGTH/2, CROWSNESTHOLE_DEPTH], center = true);
       }
    }
    // FOR UV LED
    color([1,0,1])translate([-BASE_XY/4, 0, CROWSNEST_HEIGHT/2])difference(){
        difference(){
            cube([CROWSNEST_WIDTH, CROWSNEST_LENGTH, CROWSNEST_HEIGHT], center = true);
            translate([0, 0, CROWSNEST_HEIGHT-CROWSNESTHOLE_DEPTH])cube([CROWSNESTHOLE_WIDTH, CROWSNEST_LENGTH+1, CROWSNESTHOLE_DEPTH], center = true);
        translate([CROWSNEST_WIDTH/4, 0, CROWSNEST_HEIGHT-CROWSNESTHOLE_DEPTH])cube([CROWSNEST_WIDTH-CROWSNESTHOLE_WIDTH/2, CROWSNESTHOLE_LENGTH/2, CROWSNESTHOLE_DEPTH], center = true);
       }
    }
}