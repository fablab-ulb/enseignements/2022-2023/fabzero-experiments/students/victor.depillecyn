
//PARAMETRES BOITE
BOITE_X = 100;
BOITE_Y = 70;
BOITE_Z = 100;

$fa = 5;
$fs = 0.1;
eps = 0.01;

//PARAMETRES CAPTEUR UV
// 20 x 15 x 1.5 mm


// PARAMETRES FAKECUVETTE
CUVETTE_X = 13;
CUVETTE_Y = 13;
CUVETTE_Z = 45;


// PARAMETRES BASE
BASE_XY = 50;
BASE_Z = 3;

//PARAMETRES SUPPORTS
SUPPORT_HEIGHT = 5;
SUPPORT_WIDTH = 2;
SUPPORT_LENGTH = CUVETTE_X + SUPPORT_WIDTH*2;

//PARAMETRES PAROIS
PAROIS_HEIGHT = 50;
PAROIS_WIDTH = BOITE_Y/2-CUVETTE_Y/2-SUPPORT_WIDTH/2;
PAROIS_THICKNESS = 1;

parois();
//fakecuvette();
base();
cuvette_supports();



module fakecuvette(){
    color([0,0.5,0.5], alpha = 0.2)translate([0, 0, CUVETTE_Z/2+BASE_Z])cube([CUVETTE_X, CUVETTE_Y, CUVETTE_Z], center = true);
    }
    


module base(){
    color([0.4,0.4,1])translate([0, 0, BASE_Z/2])cube([BASE_XY, BASE_XY, BASE_Z], center = true);

    }
    
module cuvette_supports(){
    //SUPPORT 1
    color([0.4,1,0.4])translate([0, CUVETTE_Y/2+SUPPORT_WIDTH/2, SUPPORT_HEIGHT/2+BASE_Z])cube([SUPPORT_LENGTH, SUPPORT_WIDTH, SUPPORT_HEIGHT], center = true);
    //SUPPORT 2 (opposes SUPPORT 1)
    mirror([0,1,0])color([0.4,1,0.4])translate([0, CUVETTE_Y/2+SUPPORT_WIDTH/2, SUPPORT_HEIGHT/2+BASE_Z])cube([SUPPORT_LENGTH, SUPPORT_WIDTH, SUPPORT_HEIGHT], center = true);
    //SUPPORT 3
    color([0.4,1,0.4])translate([CUVETTE_X/2+SUPPORT_WIDTH/2, 0, SUPPORT_HEIGHT/2+BASE_Z])rotate([0,0,90])cube([SUPPORT_LENGTH, SUPPORT_WIDTH, SUPPORT_HEIGHT], center = true);
    // SUPPORT 4 (opposes SUPPORT 3)
    mirror([1,0 ,0])color([0.4,1,0.4])translate([CUVETTE_X/2+SUPPORT_WIDTH/2, 0, SUPPORT_HEIGHT/2+BASE_Z])rotate([0,0,90])cube([SUPPORT_LENGTH, SUPPORT_WIDTH, SUPPORT_HEIGHT], center = true);
    }
    
module parois(){
    //PAROIS 1
    color([1,0,0.2])translate([0, PAROIS_WIDTH/2+CUVETTE_Y/2+SUPPORT_WIDTH/2, PAROIS_HEIGHT/2])cube([PAROIS_THICKNESS, PAROIS_WIDTH, PAROIS_HEIGHT], center = true);
    //PAROIS 2 (opposes PAROIS 1)
    color([1,0,0.2])translate([0, -PAROIS_WIDTH/2-CUVETTE_Y/2-SUPPORT_WIDTH/2, PAROIS_HEIGHT/2])cube([PAROIS_THICKNESS, PAROIS_WIDTH, PAROIS_HEIGHT], center = true);   
    }