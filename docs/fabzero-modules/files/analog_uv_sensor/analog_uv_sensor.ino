#include <Adafruit_NeoPixel.h>

//
//    FILE: UV_sensor.ino
//  AUTHOR: Rob Tillaart
// PURPOSE: demo
//    DATE: 2021-09-21
//     URL: https://github.com/RobTillaart/AnalogUVSensor
//
//  this example is not using the library,
//  sort of minimal version

Adafruit_NeoPixel pixels(1, 23, NEO_GRB + NEO_KHZ800);


void setup()
{
  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels.setPixelColor(0, pixels.Color(150, 0, 0));
  pixels.show();
  Serial.begin(115200);
  Serial.println(__FILE__);
  int NUM_INIT_MEASUREMENTS = 5;
  int INIT_MEASUREMENTS = {0, 0, 0, 0, 0};  
}


void loop(){

  while(NUM_INIT_MEASUREMENTS > 0);
    INIT_MEASUREMENTS[NUM_INIT_MEASUREMENTS] = analogRead(26));  //  assume UNO analogue port
    NUM_INIT_MEASUREMENTS = NUM_INIT_MEASUREMENTS - 1;
    delay(1000);
  Serial.println("FINISH")
}