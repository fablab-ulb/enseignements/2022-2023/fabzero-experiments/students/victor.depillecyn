# Import necessary packages
import machine
import utime
import neopixel
import math
    
# Initialise 1 neopixel on pin 23
np = neopixel.NeoPixel(machine.Pin(23), 1)

# Initialise UV sensor on pin 26
sensor = machine.ADC(26)

while True:
    measurement = sensor.read_u16()
    print(measurement)
    utime.sleep(1)