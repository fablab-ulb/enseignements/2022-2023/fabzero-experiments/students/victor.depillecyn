'''
Measure UV signal on YD-RP2040 microprocessor

Author: Victor De Pillecyn

Date of last change: 11/05/2023

License: Creative Commons BY (CC-BY)
//////////////////////////////////////////////////////////////////////////////////
'''
# Import necessary packages
import machine
import utime
import neopixel
import math
    
# Initialise 1 neopixel on pin 23
np = neopixel.NeoPixel(machine.Pin(23), 1)

# Initialise UV sensor on pin 26
sensor = machine.ADC(26)


# Define needed variables
V_0 = 400
NR_BLANKS = 100
BLANKS_PER_SEC = 100
BLANKS = []

NR_MEASUREMENTS = 100
MEASUREMENTS_PER_SEC = 100
MEASUREMENTS = []

OPL = 2 # Optical path length
MAC = 386 # Molar absorption coefficient caffeine in coffee


# User setup
#check = input("Have you put the cuvette with water in the CaféineCheck?\n")
#check = input("Have you properly closed the lid and inserted the black funnel?\n")
print("Performing blank measurement...")

# Blank measurement
while NR_BLANKS > 0:
    print(NR_BLANKS)
    measurement = sensor.read_u16()
    BLANKS.append(measurement)
    NR_BLANKS -= 1
    utime.sleep(1/BLANKS_PER_SEC)
    
# Print blanks and average blank
print(f"Blank measurements: {BLANKS}")
AVG_BLANK = round(sum(BLANKS)/len(BLANKS), 0)
print(AVG_BLANK)

# User setup
#check = input("Please change the cuvette to one with coffee?\n")
check = input("Have you properly closed the lid and inserted the black funnel?\n")
print("Performing caffeine measurement...")

# Cafeine measurement
while NR_MEASUREMENTS > 0:
    print(NR_MEASUREMENTS)
    measurement = sensor.read_u16()
    MEASUREMENTS.append(measurement)
    NR_MEASUREMENTS -= 1
    utime.sleep(1/MEASUREMENTS_PER_SEC)
    
# Print measurements and average measurement
print(f"Caffeine measurements: {MEASUREMENTS}")
AVG_MEASUREMENT = round(sum(MEASUREMENTS)/len(MEASUREMENTS), 0)
print(AVG_MEASUREMENT)

# Calculate absorption
absorbance = math.log10((AVG_BLANK-V_0)/(AVG_MEASUREMENT-V_0))
print(absorbance)

#Calculate cafeine concentration
caf_concentration = absorbance/(OPL*MAC)

