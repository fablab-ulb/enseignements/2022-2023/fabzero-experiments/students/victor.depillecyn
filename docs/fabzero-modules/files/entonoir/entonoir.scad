//https://forum.openscad.org/Best-way-to-draw-a-funnel-td9741.html


//PARAMETRES ENTONOIR
BOWL = 40;
BOWL_R = 50;
STEM = 60;
STEM_R = 4;
NECK_R = 4; // Cuvette largeur 1.25
THICKNESS = 1;
CURVE_R = 10;
PLACEMENT_ENTONOIR_Z = 80;

//DIAMETRE TROU COUVERCLE = 3 cm


//PARAMETRES BOITE
BOITE_X = 100;
BOITE_Y = 70;
BOITE_Z = 100;

$fa = 5;
$fs = 0.1;
eps = 0.01;

module profile() {
        hull() {
                translate([STEM_R, 0])
                        square([THICKNESS, eps]);

                translate([NECK_R + CURVE_R, STEM])
                        circle(r = CURVE_R);
               
                translate([BOWL_R, STEM + BOWL - eps])
                        square([THICKNESS, eps]);
        }
}

module entonoir() {
    rotate_extrude()
        difference() {
                profile();
                translate([THICKNESS, 0])
                        profile();
        }
    }
    
    
module boite(){
    cube([BOITE_X, BOITE_Y, BOITE_Z], center = true);
   
    }

translate([0, 0, PLACEMENT_ENTONOIR_Z-STEM])entonoir();
    
//translate([0, 0, BOITE_Z/2])boite();
